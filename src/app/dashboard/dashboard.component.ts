import { Component, OnInit, AnimationTransitionEvent } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {


	public _opened: boolean = false;
  public _modeNum: number = 2;
  public _positionNum: number = 0;
  public _dock: boolean = false;
  public _closeOnClickOutside: boolean = false;
  public _closeOnClickBackdrop: boolean = false;
  public _showBackdrop: boolean = false;
  public _animate: boolean = true;
  public _trapFocus: boolean = true;
  public _autoFocus: boolean = true;
  public _keyClose: boolean = false;
  public _autoCollapseHeight: number = null;
  public _autoCollapseWidth: number = null;

  public _MODES: Array<string> = ['over', 'push', 'slide'];
  public _POSITIONS: Array<string> = ['left', 'right', 'top', 'bottom'];

  public _toggleOpened(): void {
    this._opened = !this._opened;
  }

  public _toggleMode(): void {
    this._modeNum++;

    if (this._modeNum === this._MODES.length) {
      this._modeNum = 0;
    }
  }

  public _toggleAutoCollapseHeight(): void {
    this._autoCollapseHeight = this._autoCollapseHeight ? null : 500;
  }

  public _toggleAutoCollapseWidth(): void {
    this._autoCollapseWidth = this._autoCollapseWidth ? null : 500;
  }

  public _togglePosition(): void {
    this._positionNum++;

    if (this._positionNum === this._POSITIONS.length) {
      this._positionNum = 0;
    }
  }

  public _toggleDock(): void {
    this._dock = !this._dock;
  }

  public _toggleCloseOnClickOutside(): void {
    this._closeOnClickOutside = !this._closeOnClickOutside;
  }

  public _toggleCloseOnClickBackdrop(): void {
    this._closeOnClickBackdrop = !this._closeOnClickBackdrop;
  }

  public _toggleShowBackdrop(): void {
    this._showBackdrop = !this._showBackdrop;
  }

  public _toggleAnimate(): void {
    this._animate = !this._animate;
  }

  public _toggleTrapFocus(): void {
    this._trapFocus = !this._trapFocus;
  }

  public _toggleAutoFocus(): void {
    this._autoFocus = !this._autoFocus;
  }

  public _toggleKeyClose(): void {
    this._keyClose = !this._keyClose;
  }

  public _onOpenStart(): void {
    console.info('Sidebar opening');
  }

  public _onOpened(): void {
    console.info('Sidebar opened');
  }

  public _onCloseStart(): void {
    console.info('Sidebar closing');
  }

  public _onClosed(): void {
    console.info('Sidebar closed');
  }

  public _onTransitionEnd(): void {
    console.info('Transition ended');
  }
  

  tiles = [
    {class: 'metrostyle orgmetro', faClass:'fa fa-users', num: 7, title:'User'},
    {class: 'metrostyle eenmetro', faClass:'fa fa-cogs', num: 71, title:'User Logging '},
    {class: 'metrostyle metrostylelarge  boometro', faClass:'fa fa-hospital-o', num: 37, title:'Hospitals'},
    {class: 'metrostyle reemetro', faClass:'fa fa-medkit', num: 28, title:'Branches'},
    {class: 'metrostyle yoometro', faClass:'fa fa-stethoscope', num: 17, title:'Clinics'},
    {class: 'metrostyle reemetro', faClass:'fa fa-globe', num: 35, title:'Cities'},
    {class: 'metrostyle metrostylelarge  toometro', faClass:'fa fa-ticket', num: 7, title:'Reservation Type'},
    {class: 'metrostyle yoometro', faClass:'fa fa-vimeo-square', num: 9, title:'Visit Type'},
    {class: 'metrostyle toometro', faClass:'fa fa-flask', num: 27, title:'Labs'},
    {class: 'metrostyle orgmetro', faClass:'fa fa-bookmark', num: 28, title:'Rads'},
    {class: 'metrostyle eenmetro', faClass:'fa fa-heartbeat', num: 27, title:'Samples'},
    {class: 'metrostyle yoometro', faClass:'fa fa-plus-square-o', num: 87, title:'Drug'},
    {class: 'metrostyle eenmetro', faClass:'fa fa-glass', num: 342, title:'Dose'},
    {class: 'metrostyle metrostylelarge reemetro', faClass:'fa fa-map-marker', num: 28, title:'Unaided '}
  ];

  public value='';
  constructor() { }

  ngOnInit() {
  }

  curProduct(val){
    this.value=val;
  }
}
