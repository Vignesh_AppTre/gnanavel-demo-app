import { NgModule } from '@angular/core';
import {RouterModule, Routes}  from '@angular/router';



import {DashboardComponent} from './dashboard/dashboard.component';
import {DetailPageComponent} from './detail-page/detail-page.component';

const appRoutes: Routes = [
    {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
    {path: 'dashboard', component: DashboardComponent},
    {path: 'detail-page/:name', component: DetailPageComponent}
]

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports:[
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
